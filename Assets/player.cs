﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class player : MonoBehaviour {
	private float timeToChangeDirection;
	private Vector3 targetAngles;

	private GameObject zombieObject;
	private bool eat_chance = false;

	public Animator anim;
	public Rigidbody rbody;
	public float moveSpeed =2f;
	public float chaseSpeed = 0.5f;
	public Transform track;
	public float smooth = 1f;



	// Use this for initialization
	void Start () {

		rbody = GetComponent<Rigidbody>();
		anim = GetComponent<Animator>();
		ChangeDirection();
		zombieObject = GameObject.Find ("zombie");

		
		
	}
	
	// Update is called once per frame
	void Update () {
		timeToChangeDirection -= Time.deltaTime;

		//Pressing"space" to call the zombies to eat
		if (Input.GetKey (KeyCode.Space)) {    

			track.position = new Vector3(0,0,0);
			var heading = track.position - transform.position;
			var distance = heading.magnitude;
			var direction = heading / distance; //normalized direction
			float rotationSpeed = 5f;

			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);
			transform.position += transform.forward * Time.deltaTime * moveSpeed;  //move to the target

		
		}

		if (Input.GetKeyUp ("space")) {
			track.position = new Vector3(0,10,0);
		
		}

		
		if (timeToChangeDirection <= 0) {
			ChangeDirection();
		}

		
		
	}
	

	

	
	
	
	
	private void ChangeDirection() {
		
		
		float moveX = Random.Range (-20,20);
		float moveZ = Random.Range (-20,10);

		anim.Play ("walk",-1,0f);

		targetAngles = transform.eulerAngles + 180f* Vector3.up;
		transform.eulerAngles = Vector3.Lerp(transform.eulerAngles,targetAngles,smooth*Time.deltaTime);
		float move = moveSpeed * Time.time; //velocity per second
		int n = Random.Range (0,3);
		if (n == 0) {
			transform.Rotate (new Vector3(0,Time.deltaTime*-100,0));

		} 

		else if (n == 1) {
			transform.Rotate (new Vector3(0,Time.deltaTime*0,0));

		} 

		else {
			transform.Rotate (new Vector3(0,Time.deltaTime*300,0));

		}
		transform.position += transform.forward * Time.deltaTime * moveSpeed;

	
		// Zombies cannot move out the ground
		if(transform.position.z >=14 || transform.position.x>=14){

			var heading = track.position - transform.position;
			var distance = heading.magnitude;
			var direction = heading / distance; //normalized direction
			float rotationSpeed = 5f;
			transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);
			transform.position -= transform.forward * Time.deltaTime * moveSpeed;
			
		}
		
		timeToChangeDirection = 3f;
		
	}
	
	
}
